#!/usr/bin/fish

for f in (ls ./coffee/)
	echo "compiling $f"
	coffee -o js/(py -c "print('$f'.split('.')[0])").js -c coffee/$f
	if [ $status -ne 0 ]
		printf "failed to compile file $f.\nterminating remaining file compilation list.\n"
		exit 1
	end
end
exit 0
