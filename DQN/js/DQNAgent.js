// Generated by CoffeeScript 2.5.0
(function() {
  var DQNAgent, deepqlearn, fs;

  deepqlearn = require('./deepqlearn');

  fs = require('fs');

  DQNAgent = class DQNAgent {
    constructor(input_shape, output_count, input_range) {
      var i, len, v;
      this.num_inputs = 1; // def
// run on input shape (exmple: [2,2,3] (2x2 image of rgb))
      for (i = 0, len = input_shape.length; i < len; i++) {
        v = input_shape[i];
        this.num_inputs *= v; // multiply by each dimention
      }
      this.inp_range = input_range; // the possible value range for the input (example: an rgb pixle has a 255 input range)
      this.num_actions = output_count; // how many actions the agent has
      this.temporal_window = 0; // how much action memory does the agent have (the haigher the memory, the larger the network (more training time))
      this.net_size = this.num_inputs * this.temporal_window + this.num_actions * this.temporal_window + this.num_inputs; // the total input size of the network (the pervious @temporal_window states and actions and the new state)
      this.layer_defs = [
        {
          type: "input",
          out_sx: 1,
          out_sy: 1,
          out_depth: this.net_size // input layer (cus' we dont use conv2d, the input is 1d (1x1x@net_size))
        },
        {
          type: "fc",
          num_neurons: 50,
          activation: 'relu' // two hidden layers
        },
        {
          type: "fc",
          num_neurons: 50,
          activation: 'relu'
        },
        {
          type: "regression",
          num_neurons: this.num_actions // the output layer must be a regression
      // the layers
        }
      ];
      this.tdtrainer_options = {
        learning_rate: 0.001,
        momentum: 0.0,
        batch_size: 64,
        l2_decay: 0.01 // the network trainer and  train settings
      };
      this.opt = {
        temporal_window: this.temporal_window, // temporal_window ^
        experience_size: 30000, // how many steps does the agent need to keep (from training)
        start_learn_threshold: 1000, // minimum amount of experience steps to start learning
        gamma: 0.7, // how much 'plan ahead' the agnet should do (what?? what is that??)
        learning_steps_total: 2e9, // maximum amount of training steps (backpropagations) to do
        learning_steps_burnin: 3e3, // at start, do random 3k steps
        epsilon_min: 0.05, // the lowest the epsilon gets to
        epsilon_test_time: 0, // waht epsilon to use at test time
        layer_defs: this.layer_defs, // layers
        tdtrainer_options: this.tdtrainer_options // trainer
// options for the dqn agent
      };
      this.brain = new deepqlearn.Brain(this.num_inputs, this.num_actions, this.opt); // create the BRAIN
      this.rewards = []; // to keep track of the rewards and actions
      this.actions = [];
    }

    
      // predicts an action using given x state, gets the reward using given reward function
    predict(x, reward_func) {
      var action, reward;
      x = x.flat(2e308); // reshape the array to a 1d array
      x.map((function(v) {
        return v / this.inp_range; // divide each value by input range, limiting to a [0,1] range
      }));
      action = this.brain.forward(x); // get predicted action
      reward = reward_func(action); // get reward of the action from the reward_func
      this.brain.backward(reward); // backpropegate the reward
      this.rewards.push(reward); // save the rewards and the actions
      this.actions.push(action);
      return action;
    }

    predict_x(x) {
      var action;
      x = x.flat(2e308); // reshape the array to a 1d array
      x.map((function(v, self) {
        return v / self.inp_range; // divide each value by input range, limiting to a [0,1] range
      }));
      action = this.brain.forward(x, this); // get predicted action
      this.actions.push(action);
      return action;
    }

    backward(reward) {
      this.brain.backward(reward); // backpropegate the reward
      return this.rewards.push(reward); // save the rewards and the actions
    }

    
      // saves the network to the given file
    save_net(file_name) {
      var act, j, r, t;
      j = this.brain.value_net.toJSON(); // get json of net
      t = JSON.stringify(j); // string it
      fs.writeFileSync(file_name + ".net", t, "utf8"); // wirte to file
      r = JSON.stringify(this.rewards);
      fs.writeFileSync(file_name + ".reward", r, "utf8");
      act = JSON.stringify(this.actions);
      return fs.writeFileSync(file_name + ".actions", act, "utf8");
    }

    
      // loads a network from the given file
    load_net(file_name) {
      var j, t;
      t = fs.readFileSync(file_name + ".net", "utf8"); // read file
      j = JSON.parse(t); // parse it to json
      return this.brain.value_net.fromJSON(j); // set the network to it
    }

  };

  module.exports = DQNAgent;

}).call(this);
