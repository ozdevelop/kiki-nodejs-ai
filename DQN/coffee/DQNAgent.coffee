deepqlearn = require('./deepqlearn');

fs = require('fs');
# input_shape, output_count, input_range

class DQNAgent
    constructor: (input_shape, output_count, input_range) ->
        @num_inputs = 1 # def
        for v in input_shape # run on input shape (exmple: [2,2,3] (2x2 image of rgb))
            @num_inputs *= v # multiply by each dimention
            
        @inp_range = input_range # the possible value range for the input (example: an rgb pixle has a 255 input range)
        
        @num_actions = output_count # how many actions the agent has
        
        @temporal_window = 0 # how much action memory does the agent have (the haigher the memory, the larger the network (more training time))
        
        @net_size = @num_inputs*@temporal_window + @num_actions*@temporal_window + @num_inputs # the total input size of the network (the pervious @temporal_window states and actions and the new state)
        
        @layer_defs = [
            {type:"input", out_sx:1, out_sy:1,out_depth:@net_size}, # input layer (cus' we dont use conv2d, the input is 1d (1x1x@net_size))
            {type:"fc",num_neurons: 50, activation:'relu'}, # two hidden layers
            {type:"fc",num_neurons: 50, activation:'relu'},
            {type:"regression",num_neurons: @num_actions} # the output layer must be a regression
        ] # the layers
        
        @tdtrainer_options = {learning_rate:0.001, momentum:0.0, batch_size:64, l2_decay:0.01} # the network trainer and  train settings
        
        @opt = {
            temporal_window : @temporal_window, # temporal_window ^
            experience_size : 30000, # how many steps does the agent need to keep (from training)
            start_learn_threshold : 1000, # minimum amount of experience steps to start learning
            gamma : 0.7, # how much 'plan ahead' the agnet should do (what?? what is that??)
            learning_steps_total : 2e9, # maximum amount of training steps (backpropagations) to do
            learning_steps_burnin : 3e3, # at start, do random 3k steps
            epsilon_min : 0.05, # the lowest the epsilon gets to
            epsilon_test_time : 0, # waht epsilon to use at test time
            layer_defs : @layer_defs, # layers
            tdtrainer_options : @tdtrainer_options, # trainer
        } # options for the dqn agent
        
        @brain = new deepqlearn.Brain(@num_inputs,@num_actions,@opt) # create the BRAIN
        
        @rewards = [] # to keep track of the rewards and actions
        @actions = []
    
    # predicts an action using given x state, gets the reward using given reward function
    predict: (x,reward_func) ->
        
        x = x.flat(Infinity) # reshape the array to a 1d array
        
        x.map(((v) -> 
            return v/@inp_range
        )) # divide each value by input range, limiting to a [0,1] range
        
        action = @brain.forward(x) # get predicted action
        
        reward = reward_func(action) # get reward of the action from the reward_func

        @brain.backward(reward) # backpropegate the reward
        
        @rewards.push reward # save the rewards and the actions
        @actions.push action
        
        return action
    
    predict_x: (x) ->
        
        x = x.flat(Infinity) # reshape the array to a 1d array
        
        x.map(((v,self) -> 
            return v/self.inp_range
        )) # divide each value by input range, limiting to a [0,1] range
        
        action = @brain.forward(x,@) # get predicted action
        
        @actions.push action
        
        return action
    
    backward: (reward) ->
        @brain.backward(reward) # backpropegate the reward
        
        @rewards.push reward # save the rewards and the actions
    
    # saves the network to the given file
    save_net: (file_name) ->
        j = @brain.value_net.toJSON() # get json of net
        t = JSON.stringify(j) # string it
        fs.writeFileSync(file_name+".net",t,"utf8") # wirte to file
        
        r = JSON.stringify(@rewards)
        fs.writeFileSync(file_name+".reward",r,"utf8")
    
        act = JSON.stringify(@actions)
        fs.writeFileSync(file_name+".actions",act,"utf8")
    
    
    # loads a network from the given file
    load_net: (file_name) ->
        t = fs.readFileSync(file_name+".net","utf8") # read file
        j = JSON.parse(t) # parse it to json
        @brain.value_net.fromJSON(j) # set the network to it
        
    
module.exports = DQNAgent